// ============================================================================
//
// = CONTEXT
//    TANGO Project - Fluke Configuration
//
// = FILENAME
//    FlukeConfiguration.h
//
// = AUTHOR
//    X. Elattaoui
// ============================================================================

#ifndef _FLUKE_CONFIG_H_
#define _FLUKE_CONFIG_H_
#include <string>

/**
*  \brief The default configuration sent if the property is empty
*
*  \author Xavier Elattaoui
*  \date 10-2019
*/
namespace Fluke8508A_ns
{

//- commented line starts with a specific char
static std::string COMMENT_LINE   = "#";
//- Fluke commands CONFIGURATION
static std::string INPUT_FRONT    = "INPUT REAR";
static std::string RANGE          = "DCV AUTO";     //- range : Auto
static std::string RESOLUTION     = "DCV RESL8";    //- resolution of the measurement : 8,5 digits
static std::string FILTER_ON      = "DCV FILT_ON";  //- inserts a hardware analog filter into the signal path
static std::string FILTER_OFF     = "DCV FILT_OFF";
static std::string FAST_ON        = "DCV FAST_ON";  //- Reduces the A-D integration time, for faster conversions
static std::string FAST_OFF       = "DCV FAST_OFF";
static std::string EXTERNAL_GUARD = "GUARD EXT";

} //- end namespace

#endif // _FLUKE_CONFIG_H_
