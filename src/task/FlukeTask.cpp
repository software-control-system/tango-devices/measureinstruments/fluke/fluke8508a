// ============================================================================
//
// = CONTEXT
//    TANGO Project - FlukeTask Task Class
//
// = FILENAME
//    FlukeTask.cpp
//
// = AUTHOR
//    X. Elattaoui
// ============================================================================
#include "FlukeConfiguration.h"
#include "task/FlukeTask.hpp"
#include <yat/utils/XString.h>
#include <yat/time/Timer.h>     //- perf

// ----------------------------------------------------------------------------
//- the YAT user messages 
const std::size_t RESET               = yat::FIRST_USER_MSG + 1;
const std::size_t ENABLE_FAST         = yat::FIRST_USER_MSG + 10;
const std::size_t DISABLE_FAST        = yat::FIRST_USER_MSG + 11;
const std::size_t ENABLE_FILTER       = yat::FIRST_USER_MSG + 20;
const std::size_t DISABLE_FILTER      = yat::FIRST_USER_MSG + 21;
const std::size_t CLEAR_ERRORS        = yat::FIRST_USER_MSG + 30;
// ----------------------------------------------------------------------------
//- thread polling periods
/* NOTE: 
  To configure period according to resolution (for DCV),
    take a look at the table 4-2 page 113/212.
*/
//- default period
const std::size_t kPERIODIC_TMOUT_MS = 2000;  //- task period = in ms
//- if resolution is 7 digits
const std::size_t kPERIODIC_RESL7_MS = 6000;
//- if resolution is 8 digits
const std::size_t kPERIODIC_RESL8_MS = 12000;
// ----------------------------------------------------------------------------
//- READ back commands
//- CMD INFO : performing an Execute Trigger (*TRG;RDG?) followed by a reading query. X? is intended for high speed use.
static std::string READ_DATA_CMD      = "*TRG;RDG?";//"X?"
//- ERRORS commands
//- recalls the last error from the queue of device-dependent errors
static std::string DEVICE_ERROR       = "DDQ?"; //- returns 0 if no error
//- recalls the last error from the queue of execution errors 
static std::string EXECUTION_ERROR    = "EXQ?"; //- returns 0 if no error
//- reset Fluke device
static std::string RESET_CMD          = "*RST";
// ----------------------------------------------------------------------------
//- used to convert voltage in current value
static const double IMAX              = 10.;    //- Volts

namespace Fluke8508A_ns
{
// ============================================================================
// FlukeTask::FlukeTask
// ============================================================================
FlukeTask::FlukeTask (Tango::DeviceImpl * host_device,
                      std::string com_link_name,              //- device which handles the communication protocol
                      std::vector<std::string> configuration) //- HW IP address
: yat4tango::DeviceTask(host_device),
  m_hostDev(host_device),
  m_comLinkName(com_link_name),
  m_comLink(0),
  m_voltage(yat::IEEE_NAN),
  m_current(yat::IEEE_NAN),
  m_factor(1.),
  m_dcctRange(10.),
  m_nbTurns(1.),
  m_fullScale(IMAX),
  m_state(Tango::INIT),
  m_status("Init on the way"),
  m_error(""),
  m_polling_period(kPERIODIC_TMOUT_MS)
{
  INFO_STREAM << "\t\t FlukeTask::FlukeTask <-" << std::endl;

  m_configuration = configuration;
  
  //- compute voltage to current factor conversion
  m_factor = (m_dcctRange / m_nbTurns) / m_fullScale;
  
  //- check resolution to set polling period
  for( std::size_t i=0; i < m_configuration.size(); i++ )
  {
    if( m_configuration.at(i).find("RESL7") != std::string::npos )
    {
      m_polling_period = kPERIODIC_RESL7_MS;
    } 
    else if( m_configuration.at(i).find("RESL8") != std::string::npos )
    {
      m_polling_period = kPERIODIC_RESL8_MS;
    }
    else
    {
      m_polling_period = kPERIODIC_TMOUT_MS;
    }
  }

  INFO_STREAM << "\t\t FlukeTask::FlukeTask -> m_polling_period = " << m_polling_period << std::endl;
}

// ============================================================================
// FlukeTask::~FlukeTask
// ============================================================================
FlukeTask::~FlukeTask (void)
{
 INFO_STREAM << "\t\t FlukeTask::~FlukeTask <-" << std::endl;

 INFO_STREAM << "\t\t FlukeTask::~FlukeTask ->" << std::endl;
}

// ============================================================================
// FlukeTask::reset
// ============================================================================
void FlukeTask::reset()
{
  //- prepare msg
  yat::Message * msg = new yat::Message(RESET, MAX_USER_PRIORITY);
  
  post(msg);
}

// ============================================================================
// FlukeTask::enable_filter
// ============================================================================
void FlukeTask::enable_filter()
{
  //- prepare msg
  yat::Message * msg = new yat::Message(ENABLE_FILTER, MAX_USER_PRIORITY);
  
  post(msg);
}

// ============================================================================
// FlukeTask::disable_filter
// ============================================================================
void FlukeTask::disable_filter()
{
  //- prepare msg
  yat::Message * msg = new yat::Message(DISABLE_FILTER, MAX_USER_PRIORITY);
  
  post(msg);
}

// ============================================================================
// FlukeTask::enable_fast
// ============================================================================
void FlukeTask::enable_fast()
{
  //- prepare msg
  yat::Message * msg = new yat::Message(ENABLE_FAST, MAX_USER_PRIORITY);
  
  post(msg);
}

// ============================================================================
// FlukeTask::disable_fast
// ============================================================================
void FlukeTask::disable_fast()
{
  //- prepare msg
  yat::Message * msg = new yat::Message(DISABLE_FAST, MAX_USER_PRIORITY);
  
  post(msg);
}

void FlukeTask::clear_errors()
{
  //- prepare msg
  yat::Message * msg = new yat::Message(CLEAR_ERRORS, MAX_USER_PRIORITY);
  
  post(msg);
}

//-----------------------------------------------
//- the user core of the Task -------------------
void FlukeTask::process_message (yat::Message& _msg) throw (Tango::DevFailed)
{
  DEBUG_STREAM << "FlukeTask::process_message::receiving msg " << _msg.to_string() << std::endl;

  //- handle msg
  switch (_msg.type())
  {
    //- THREAD_INIT =======================
  case yat::TASK_INIT:
    {
      DEBUG_STREAM << "FlukeTask::process_message::THREAD_INIT::thread is starting up" << std::endl;
      //- "initialization" code goes here
      try
      {
        yat::Timer t;
        //- 
        create_proxy();
        //- configure device
        send_configuration();
        //- configure task
        enable_timeout_msg(false);
        enable_periodic_msg(true);
        set_periodic_msg_period(m_polling_period);

        INFO_STREAM << "\t FlukeTaskMgr::TASK_INIT -> DONE in " << t.elapsed_msec() << " milliseconds" << std::endl;
      }
      catch (std::bad_alloc&)
      {
        yat::MutexLock guard(m_error_mutex);
        m_error += "FlukeTask::process_message OUT_OF_MEMORY cannot allocate device proxy ";
        ERROR_STREAM << m_error << std::endl;
        return;
      }
      catch(...)
      {
        yat::MutexLock guard(m_error_mutex);
        m_error += " exception ... caught trying to initialise FlukeTask.";
        ERROR_STREAM << m_error << std::endl;
        return;
      }
    } 
    break;
    //- TASK_EXIT =======================
  case yat::TASK_EXIT:
    {
      DEBUG_STREAM << "\n\n\t\tFlukeTask::process_message handling TASK_EXIT thread is quitting ..." << std::endl;

      delete_proxy();
      
      DEBUG_STREAM << "\t\t EXIT Done!" << std::endl;
    }
    break;
    //- TASK_PERIODIC ===================
  case yat::TASK_PERIODIC:
    {
      DEBUG_STREAM << "\n\n\t\tFlukeTask::process_message entering handling of TASK_PERIODIC msg" << std::endl;
      //- code relative to the task's periodic job goes here

      yat::Timer t;

      do_periodic_job();
      
      //- check polling period
      std::size_t time_to_execute_periodic_job = t.elapsed_msec();
      if ( time_to_execute_periodic_job > m_polling_period )
      {
        //- add 10%
        m_polling_period = time_to_execute_periodic_job + (time_to_execute_periodic_job*0.1);
        //- set new task polling period
        set_periodic_msg_period(m_polling_period);
        INFO_STREAM << "\n\n\t\tFlukeTask::process_message NEW polling period value = " << m_polling_period << std::endl;
      }

      INFO_STREAM << "**********\t\tFlukeTask::process_message TIME TO COMPLETE TASK_PERIODIC : " << t.elapsed_msec() << " milliseconds \n\n\n" << std::endl;
    }
    break;
    //- USER_DEFINED_MSG ================
  case RESET:
    {
      //- reset device
      write(RESET_CMD);
      //- reconfigure device
      send_configuration();
    }
    break;
  case ENABLE_FILTER:
    {
      write(FILTER_ON);
    }
    break;
  case DISABLE_FILTER:
    {
      write(FILTER_OFF);
    }
    break;
  case ENABLE_FAST:
    {
      write(FAST_ON);
    }
    break;
  case DISABLE_FAST:
    {
      write(FAST_OFF);
    }
    break;
  case CLEAR_ERRORS:
    {
      yat::MutexLock guard(m_error_mutex);
      m_error.clear();
    }
    break;
  } //- switch (_msg.type())
} //- FlukeTask::process_message

// ============================================================================
// FlukeTask::do_periodic_job
// ============================================================================
void FlukeTask::do_periodic_job()
{
  try
  {
    //- read back voltage value
    std::string resp = write_read(READ_DATA_CMD);
    {//- critical section
      yat::MutexLock guard(m_data_mutex);
      if( !resp.empty() )
      {
        //- convert value from string
        m_voltage = yat::XString<double>::to_num(resp);
        //- compute factor
        m_factor = ( (m_dcctRange / m_nbTurns) / m_fullScale );
        //- convert voltage to current value
        m_current = m_voltage * m_factor;
      }
      else
      {
        m_voltage = yat::IEEE_NAN;
        m_current = yat::IEEE_NAN;
      }
      INFO_STREAM << "\t PERIODIC : " << std::endl;
      INFO_STREAM << "\t\t voltage = " << m_voltage << std::endl;
      INFO_STREAM << "\t\t factor  = " << m_factor << std::endl;
      INFO_STREAM << "\t\t current = " << m_current << std::endl;
    }
    //- update state/status
    update_state_status();
  }
  catch(Tango::DevFailed& df)
  {
    ERROR_STREAM << "Failed to read back voltage value :\n" << df << std::endl;
    //- invalid data
    yat::MutexLock guard(m_data_mutex);
    m_voltage = yat::IEEE_NAN;
    m_current = yat::IEEE_NAN;
  }
}

// ============================================================================
// FlukeTask::update_state_status
// ============================================================================
void FlukeTask::update_state_status ()
{
  std::string status("");
  Tango::DevState state = Tango::UNKNOWN;
  
  std::string dev_err_str("");
  std::string exe_err_str("");
  
  //- check communication device name
  if ( m_comLinkName.empty() )
  {
    state  = Tango::FAULT;
    status = "CommunicationLink property is empty!";
  }
  //- check proxy
  else if ( !m_comLink )
  {
    state  = Tango::FAULT;
    status = "Device initialization failed : no proxy created.";
  }
  else if ( !m_error.empty() )
  {
    yat::MutexLock guard(m_error_mutex);
    state = Tango::FAULT;
    status= m_error;
  }
  //- if no error, read back Fluke error
  if ( state != Tango::FAULT )
  {
    //- check device dependant error
    Tango::DevShort dev_err = read_device_dependent_errors(dev_err_str);
    if ( dev_err )
    {
      state  = Tango::ALARM;
      status = "Device dependent error num : " + dev_err_str + " raised.";
    }

    //- check device execution error
    Tango::DevShort exe_err = read_device_execution_errors(exe_err_str);
    if ( exe_err )
    {
      state  = Tango::ALARM;
      status = "Device execution error num : " + exe_err_str + " raised.";
    }
    
    if( !dev_err && !exe_err )
    {
      state  = Tango::ON;
      status = "No error.";
    }
  }
  
  { //- critical section
    yat::MutexLock guard(m_state_status_mutex);
    m_status = status;
    m_state	 = state;
  }
}

// ============================================================================
// FlukeTask::read_device_dependent_errors:
// ============================================================================
Tango::DevShort FlukeTask::read_device_dependent_errors(std::string& deverr_num_str)
{
  Tango::DevShort error_type = 0;

  deverr_num_str = write_read(DEVICE_ERROR);
  
  error_type = yat::XString<short>::to_num(deverr_num_str);

  return error_type;
}

// ============================================================================
// FlukeTask::read_device_execution_errors:
// ============================================================================
Tango::DevShort FlukeTask::read_device_execution_errors(std::string& err_num_str)
{
  Tango::DevShort error_type = 0;

  err_num_str = write_read(EXECUTION_ERROR);
  
  error_type = yat::XString<short>::to_num(err_num_str);

  return error_type;
}

// ============================================================================
// FlukeTask::send_configuration :
// ============================================================================
void FlukeTask::send_configuration()
{
  std::size_t size = m_configuration.size();
  INFO_STREAM << "FlukeTask::send_configuration() -> entering..." << std::endl;

  if ( m_comLink )
  {
    for ( std::size_t i=0; i < size; i++ )
    {
      std::string cmd = m_configuration.at(i);
  INFO_STREAM << i << ") cmd : " << cmd ;
      //- is a comment
      if ( cmd.find(COMMENT_LINE) != std::string::npos )
        continue;
      write( cmd );
  INFO_STREAM << " sent." << std::endl;
    }
  }
  
  INFO_STREAM << "FlukeTask::send_configuration() -> done!" << std::endl;
}

// ============================================================================
// FlukeTask::write
// ============================================================================
void FlukeTask::write(std::string cmd_to_send)
{
  std::string response("");

  try
  {
    if ( m_comLink )
    {
      m_comLink->command_in("Write", cmd_to_send);
      DEBUG_STREAM << "cmd \"" << cmd_to_send << "\" sent." << std::endl;
    }
  }
  catch(Tango::DevFailed& df)
  {
    yat::MutexLock guard(m_error_mutex);
    m_error = "Failed to write command \"" + cmd_to_send + "\":\n" + std::string(df.errors[0].desc);
    ERROR_STREAM << m_error << std::endl;
  }
}

// ============================================================================
// FlukeTask::write_read
// ============================================================================
std::string FlukeTask::write_read(std::string cmd_to_send)
{
  std::string response("");

  try
  {
    //- send commannd
    if ( m_comLink )
    {
      m_comLink->command_inout("WriteRead", cmd_to_send, response);
      DEBUG_STREAM << "WriteRead -> resp : \"" << response << "\" for cmd : \"" << cmd_to_send << "\"" << std::endl;
    }
    //- check response
    if( response.empty() )
    {
      THROW_DEVFAILED( "NO_RESPONSE_RECEIVED",
                              "Received empty response from instrument",
                              "FlukeTask::write_read" );
    }
  }
  catch(Tango::DevFailed& df)
  {
    yat::MutexLock guard(m_error_mutex);
    m_error = "Failed to write_read command \"" + cmd_to_send + "\":\n" + std::string(df.errors[0].desc);
    ERROR_STREAM << m_error << std::endl;
  }

  return response;
}

// ============================================================================
// FlukeTask::create_proxy
// ============================================================================
void FlukeTask::create_proxy()
{
  if ( !m_comLinkName.empty() )
  {
    try
    {
      m_comLink = new Tango::DeviceProxyHelper(m_comLinkName, m_hostDev);

      Tango::DevState linkState = Tango::UNKNOWN;
      m_comLink->command_out("state", linkState);

      if ( linkState != Tango::ON )
      {
        delete_proxy();
        FATAL_STREAM << "Communication link state is not ON (or opened)." << std::endl;
      }
    }
    catch(Tango::DevFailed& df)
    {
      FATAL_STREAM << "FATAL create_proxy failed :\n" << df << std::endl;
      delete_proxy();
    }
  }
}

// ============================================================================
// FlukeTask::delete_proxy
// ============================================================================
void FlukeTask::delete_proxy ()
{
  if ( m_comLink )
  {
    delete m_comLink;
    m_comLink = 0;
  }
}

} //- end namespace
