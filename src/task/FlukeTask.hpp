// ============================================================================
//
// = CONTEXT
//    TANGO Project - FlukeTask Task Class
//
// = FILENAME
//    FlukeTask.hpp
//  This class encapsulates the ADC access over a TCP socket to the FBT V3 electronic crates 
// All call to the underlying socket access library must be done by this class to guarantee the access is serialized
// This imposed that there is only 1 DataManager per BBFDataviewer device (i.e this class should be a Singleton)
// 
// = AUTHOR
//    X. Elattaoui
// ============================================================================

#ifndef _FLUKE_TASK_H_
#define _FLUKE_TASK_H_

#include <tango.h>
#include <string>
#include <vector>
#include <yat4tango/DeviceTask.h>
#include <DeviceProxyHelper.h>

/**
*  \brief Class to manage Fluke's data
*
*  \author Xavier Elattaoui
*  \date 10-2019
*/
namespace Fluke8508A_ns
{

class FlukeTask : public yat4tango::DeviceTask
{
public:

  /**
  *  \brief Initialization.
  */
  FlukeTask ( Tango::DeviceImpl * host_device,
              std::string com_link_name,              //- device which handles the communication protocol
              std::vector<std::string> configuration); //- HW IP address

  /**
  *  \brief Release resources.
  */
  virtual ~FlukeTask();
  
  /**
  *  \brief Getters/Setters.
  */
  double get_voltage() {
    yat::MutexLock guard(m_data_mutex);
    return m_voltage;
  }
  
  double get_current() {
    yat::MutexLock guard(m_data_mutex);
    return m_current;
  }
  
  double get_factor() {
    yat::MutexLock guard(m_data_mutex);
    return m_factor;
  }
  
  double get_dcctRange() {
    yat::MutexLock guard(m_data_mutex);
    return m_dcctRange;
  }
  
  void set_dcctRange(double dcctRange) {
    yat::MutexLock guard(m_data_mutex);
    m_dcctRange = dcctRange;
  }
  
  double get_nbTurns() {
    yat::MutexLock guard(m_data_mutex);
    return m_nbTurns;
  }
  
  void set_nbTurns(double nbTurns) {
    yat::MutexLock guard(m_data_mutex);
    m_nbTurns = nbTurns;
  }
  
  double get_fullScale() {
    yat::MutexLock guard(m_data_mutex);
    return m_fullScale;
  }
  
  void set_fullScale(double fullScale) {
    yat::MutexLock guard(m_data_mutex);
    m_fullScale = fullScale;
  }
  
  /**
  *  \brief Commands.
  */
  void reset();
  
  void enable_filter ();
  
  void disable_filter();
  
  void enable_fast ();
  
  void disable_fast();
  
  /**
  *  \brief State/Status.
  */
  Tango::DevState get_state_status(std::string& status) {
    yat::MutexLock guard(m_state_status_mutex);
    status = m_status;
    return m_state;
  }

  /**
  *  \brief Error(s).
  */
  std::string get_error() {
    yat::MutexLock guard(m_error_mutex);
    return m_error;
  }
  
  void clear_errors();

protected:
    
  yat::Mutex  m_data_mutex;
  yat::Mutex  m_state_status_mutex;
  yat::Mutex  m_error_mutex;

  //- process_message (implements yat4tango::DeviceTask pure virtual method)
  virtual void process_message (yat::Message& msg)
    throw (Tango::DevFailed);

private:

  Tango::DevShort read_device_dependent_errors(std::string& deverr_num_str);
  Tango::DevShort read_device_execution_errors(std::string& err_num_str);
  
  void create_proxy();
  void delete_proxy();

  void send_configuration();

  void write (std::string);
  std::string write_read(std::string);

  void do_periodic_job ();
  
  void update_state_status();
  
  //- the host device 
  Tango::DeviceImpl * m_hostDev;
  
  //- communication link
  std::string m_comLinkName;
  Tango::DeviceProxyHelper* m_comLink;
  
  double m_voltage;
  
  double m_current;
  
  double m_factor;
  
  double m_dcctRange;
  
  double m_nbTurns;
  
  double m_fullScale;
  
  Tango::DevState m_state;
  std::string m_status;
  
  std::string m_error;
  
  std::size_t m_polling_period;
  
  std::vector<std::string> m_configuration;
  
};

} //- end namespace

#endif // _FLUKE_TASK_H_
